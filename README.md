# Taurie - README.md

## About me

Thanks for visiting my README! I am a Product Design Manager for [Foundations], [Integrations] and
[Data Science] at GitLab. I am also a maintainer of our [Pajamas Design System].

* I live in Portland, Oregon and love the Pacific Northwest. 
* My time zone is
[UTC-8](https://www.timeanddate.com/worldclock/timezone/utc-8). My working hours
are somewhere between 7:30am - 4:30pm PST but I take advantage of the flexibility that
remote work affords and incorporate personal tasks throughout the day.
* I joined GitLab in July of 2016 as a Product Designer.
* I went to art school and earned my degree in Communication Design.
* In my free time I enjoy being active outdoors by hiking, kayaking, and trying new activities.
* I am introverted and need alone time to recharge after bursts of socializing. Becoming a Manager
has meant I've become more comfortable being in meetings regularly.
* I value transparency, open source, and open design.

## My role as a Product Design Manager

As a Product Design Manager at GitLab, it is my job to:

* Support my reports with career development by identifying opportunities that connect with
organizational goals.
* Deeply understand the end-to-end product in order to assist the department in viewing
design from a holistic perspective.
* Identify strategic UX needs across the product and work crossfunctionality to address
them.
* Lead strategy and direction for the [Pajamas Design System] 
while being a major contributor.
* Identify process and workflow improvements.
* Connect and drive work towards the [product vision](https://about.gitlab.com/direction/product-vision/).

For more information on my role as a Product Designer, check out the full 
[job description](https://about.gitlab.com/job-families/engineering/product-design-management/#product-design-manager).

## Communication

I believe there is a time and place for both async and sync communication, and
I default to asyncronous first.

You can reach me through:

* **Slack:** Checked first thing daily. Considered asynchronous communication and
ideal for quick questions-answers. Only installed on my phone during times of frequent travel.
* **Email:** Checked regularly to stay up to-to-date with issues and merge requests.
Used in place of GitLab todos. 
* **GitLab Issues:** Discussions for features, bugs, proposals, etc,. Decisions
are documented here.
* **Zoom:** Ideal for conversations requiring back-and-forth in-depth dialogue.

I tend to be direct, while still being kind. I'll be the first one to speak up or end a call
to avoid awkward silence. I try to actively hold back to leave space for others
to speak up, even if the initial silence is painful for me. :)

## How I work

* Asynchronous communication provides an opportunity to truly understand the problem
at hand prior to responding. I put a lot of emphasis here and believe it allows
me to contribute in a more meaningful way.
* I enjoy improving processes and helping others. Unblocking people is a top priority
for me.
* I'm a strong advocate of boring solutions and iterative improvements. Iteration does not
equate to poor usability.
* I believe in directly responsible individuals and avoid design-by-committee.
* I prescribe to the mental model that all meetings should have an agenda in advance
and if there is no agenda, the call should be canceled.

## Values and beliefs

* Seek to understand before being understood.
* Always assume best intent.
* Great teams and products are formed through autonomy and trust.
* Leaders are not linear. They are not only your managers, but also your peers and
reports. Hierarchy does not make a leader.

[Foundations]:https://about.gitlab.com/direction/ecosystem/foundations/
[Integrations]:https://about.gitlab.com/direction/ecosystem/integrations/
[Data Science]:https://about.gitlab.com/direction/data-science/
[Pajamas Design System]:https://design.gitlab.com/
