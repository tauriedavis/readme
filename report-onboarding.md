- Send address intake form [https://docs.google.com/forms/d/e/1FAIpQLSdFiH7jSP12U6b-RFWgee1Qe3SUVfadINwj7wOnk659jywJbw/viewform?usp=sf_link](https://docs.google.com/forms/d/e/1FAIpQLSdFiH7jSP12U6b-RFWgee1Qe3SUVfadINwj7wOnk659jywJbw/viewform?usp=sf_link)
- Add team member on [GitLab Product Categories Handbook](https://about.gitlab.com/handbook/product/categories/) page
- Add team member to [teams.yml retrospective file](https://gitlab.com/gitlab-org/async-retrospectives/-/blob/master/teams.yml)
- Danger review roulette

# Ecosystem::Foundations

- Add team member to [https://gitlab.com/gitlab-com/gitlab-ux/ux-foundations](https://gitlab.com/gitlab-com/gitlab-ux/ux-foundations)
- Milestone planning [https://gitlab.com/gitlab-org/ecosystem-stage/team-tasks](https://gitlab.com/gitlab-org/ecosystem-stage/team-tasks)
- Add team member to [Retrospective project](https://gitlab.com/gl-retrospectives/ecosystem-stage/foundations-group)
- Add team member to *Ecosystem Stage Calendar* in Google Calendar
- Add team member to team Google Calendar meetings:
    - *Foundations Open Office Hours*
        - Rotating Monday / Thursday each week
    - *Foundations Milestone Planning*
- Add to slack channels
    - #s_ecosystem
    - #g_ecosystem_foundations
    - #g_ecosystem_foundations-standup
- Add team member to GitLab UI danger UX review [https://gitlab.com/gitlab-org/gitlab-ui/-/blob/main/danger/simple_ux_review/Dangerfile#L5](https://gitlab.com/gitlab-org/gitlab-ui/-/blob/main/danger/simple_ux_review/Dangerfile#L5)

# Ecosystem::Integrations

- Milestone planning [https://gitlab.com/gitlab-org/ecosystem-stage/team-tasks](https://gitlab.com/gitlab-org/ecosystem-stage/team-tasks)
- Add team member to [Retrospective project](https://gitlab.com/gl-retrospectives/ecosystem-stage/integrations-group)
- Add team member to *Ecosystem Stage Calendar* in Google Calendar
- Add team member to team Google Calendar meetings:
    - *Integrations bi-weekly meetup*
- Add to slack channels
    - #s_ecosystem
    - #g_ecosystem_integrations
    - #g_ecosystem_integrations-standup

# Data Science::Applied ML

- Milestone planning [https://gitlab.com/gitlab-org/modelops/team-tasks](https://gitlab.com/gitlab-org/modelops/team-tasks)
    - Add as direct member to [https://gitlab.com/groups/gitlab-org/modelops/applied-ml/-/group_members?with_inherited_permissions=exclude](https://gitlab.com/groups/gitlab-org/modelops/applied-ml/-/group_members?with_inherited_permissions=exclude)
- Add team member to [Retrospective project](https://gitlab.com/gl-retrospectives/modelops/applied-ml)
    - Add team member to [team.yml retro file](https://gitlab.com/gitlab-org/async-retrospectives/-/blob/master/teams.yml) to be pinged in issues
- Add team member to *ModelOps Stage Calendar* in Google Calendar
- Add team member to team Google Calendar meetings:
    - *Applied ML weekly team meeting (APAC)*
    - (Optional) *Applied ML Engineering Discussion - EMEA/APAC*
- Add to slack channels
    - #s_modelops
    - #g_modelops_standup

# Data Science::Anti-abuse

- Add team member to milestone planning issue template [https://gitlab.com/gitlab-org/modelops/anti-abuse/team-tasks/-/blob/main/.gitlab/issue_templates/planning_template.md](https://gitlab.com/gitlab-org/modelops/anti-abuse/team-tasks/-/blob/main/.gitlab/issue_templates/planning_template.md)
- Add team member to [Retrospective project](https://gitlab.com/gl-retrospectives/anti-abuse/anti-abuse)
    - Add team member to [team.yml retro file](https://gitlab.com/gitlab-org/async-retrospectives/-/blob/master/teams.yml) to be pinged in issues
- Add team member to Google Calendar meetings:
    - *Anti-abuse team sync - AMER/APAC*
    - *Anti-abuse team sync - AMER/EMEA*
- Add to slack channels
    - #s_modelops
    - #g_anti-abuse
